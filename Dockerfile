FROM node

LABEL maintainer="Trabalho"

WORKDIR /app

COPY ./app.js /app

RUN npm install express

ENTRYPOINT ["node", "app.js"]